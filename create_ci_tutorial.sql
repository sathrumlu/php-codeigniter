USE guitar_shop;  -- MySQL command

-- Drop the users table if exists
DROP TABLE IF EXISTS `news`;

-- -----------------------------------------------------
-- Table `blog`.`news` (For CodeIgniter Tutorial)
-- -----------------------------------------------------
DROP TABLE IF EXISTS `news`;

CREATE TABLE IF NOT EXISTS `news` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `title` varchar(128) NOT NULL,
        `slug` varchar(128) NOT NULL,
        `text` text NOT NULL,
        PRIMARY KEY (id),
        KEY slug (slug)
);


INSERT INTO `news` (`title`, `slug`, `text`)
VALUES ('First Post', 'fp', '<p>This is my first Post!!!</p>'),
       ('Pizza', 'pizza', '<p>Pizza</p>'),
       ('FooBar', 'fb', '<p>FooBar</p>');