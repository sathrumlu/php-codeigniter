-- create and select the database
DROP DATABASE IF EXISTS blog;
CREATE DATABASE blog;
USE blog;  -- MySQL command


-- -----------------------------------------------------
-- Table `blog`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `users`;

CREATE TABLE IF NOT EXISTS `users` (
  `username` VARCHAR(50) NOT NULL,
  `password` VARCHAR(60) NOT NULL,
  `name_f` VARCHAR(50) NOT NULL,
  `name_l` VARCHAR(50) NOT NULL,
  `is_admin` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`username`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog`.`blog_posts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog_posts` ;

CREATE TABLE IF NOT EXISTS `blog_posts` (
  `post_id` INT NOT NULL AUTO_INCREMENT ,
  `post_title` VARCHAR(100) NOT NULL ,
  `post_body` TEXT NOT NULL ,
  `post_created` TIMESTAMP NOT NULL DEFAULT current_timestamp() ,
  `username` VARCHAR(50) NOT NULL ,
  PRIMARY KEY (`post_id`))
ENGINE = InnoDB;



-- Insert data into the tables
INSERT INTO users (username, password, name_f, name_l, is_admin) VALUES
('admin', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Administrator', 'Person', 1),
('user', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'User', 'Person', 0);


-- Insert data into the tables
INSERT INTO blog_posts (post_title, post_body, username) VALUES
('Hello World', "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi scelerisque libero urna, ac cursus ipsum porta quis. Praesent elementum neque at eros dictum, et ultrices mauris aliquam. Nulla non congue lacus. Vestibulum a lacus ac nulla faucibus dignissim quis eu est. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse elementum elementum aliquet. Aenean fringilla viverra dui, ullamcorper venenatis elit condimentum a. Etiam lorem nibh, pellentesque sed diam vel, imperdiet suscipit ligula. Vestibulum molestie nisl non pellentesque aliquet.</p><p>Proin mollis pellentesque imperdiet. Praesent tempus justo in eros ultricies, at mattis est maximus. Duis tristique faucibus nisi, ac iaculis augue maximus nec. Mauris placerat purus vel elementum semper. Aenean eleifend velit mattis, lacinia enim eget, suscipit mi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc egestas tortor eu quam ornare gravida non quis felis.</p><p>Phasellus sed velit nec turpis accumsan dictum. Nullam finibus nec augue vulputate tempor. Nulla commodo nibh ut felis faucibus viverra. Aenean imperdiet pretium pharetra. Nulla id pellentesque justo. Proin egestas aliquet erat. Aliquam finibus lacus at diam porttitor fermentum.</p><p>Donec vel augue non urna interdum auctor nec ac leo. Sed ultrices neque dui, ut pretium turpis finibus quis. Aliquam nisl urna, sollicitudin ut arcu in, vestibulum convallis enim. Etiam posuere lorem ac ex blandit, eget lacinia ligula lacinia. Cras porttitor tellus id dictum tristique. Sed nec porta est, eu lobortis neque. Vestibulum sit amet consequat ipsum. Pellentesque rhoncus quis est sed pharetra. Sed faucibus libero sem, id luctus nibh porttitor sed. Maecenas ac ligula vitae lectus eleifend tincidunt. Suspendisse potenti.</p><p>Duis vitae diam sit amet risus commodo pellentesque a molestie magna. Quisque ullamcorper, mi ut pharetra placerat, purus dolor commodo massa, a faucibus est dolor et tortor. Vestibulum placerat, lacus at convallis vehicula, elit risus sagittis libero, quis semper mauris diam eget mi. Etiam quam felis, lacinia vel dui sit amet, porttitor cursus nunc. Vestibulum pulvinar enim a lobortis volutpat. Vestibulum varius at ipsum eu viverra. Nulla vehicula justo odio, sodales egestas lacus laoreet vel. Aenean vestibulum sagittis convallis. Nulla facilisi. Curabitur facilisis arcu ipsum, nec tempus massa ultricies at. Praesent porttitor justo et dolor malesuada, porta gravida odio ornare. Nam convallis augue tortor, id tempus nulla laoreet ac. Pellentesque nec mauris enim. Morbi nisl leo, lobortis sed facilisis elementum, sodales a mauris. Aenean sagittis, arcu consectetur vestibulum dignissim, tortor velit tristique neque, viverra pharetra nisi arcu at purus. Quisque ac ultrices quam.</p>", 'user'),
('FooBar', "<p>Vivamus varius at neque at tristique. Etiam in commodo sapien. Nulla pharetra quis nisi quis eleifend. Ut lobortis nunc ut libero commodo pellentesque. Donec vestibulum lobortis risus, sed fermentum magna pellentesque sed. In pulvinar nibh ac laoreet rhoncus. Aenean vitae lacus auctor, malesuada augue vitae, rhoncus eros. Ut quis dignissim ex, non auctor justo. Quisque commodo cursus tortor ut porta. Nunc ullamcorper, diam quis molestie hendrerit, velit est bibendum purus, sit amet pretium nisl nulla vestibulum mi. Cras ornare elementum dui, et commodo eros mollis quis.</p><p>Suspendisse aliquam, ante ac consectetur consectetur, tortor elit pellentesque neque, a volutpat leo ipsum quis leo. Cras vel erat ligula. Integer ut libero malesuada, egestas metus in, tempor ligula. Quisque dictum volutpat orci. Pellentesque fermentum dui nec libero molestie volutpat. Donec pellentesque, felis porttitor convallis vulputate, felis nunc rutrum felis, nec maximus dolor nisi vitae diam. Nunc neque justo, tempus vel nibh eget, condimentum hendrerit mi.</p><p>Sed quis condimentum felis, eget pharetra libero. Donec nec nibh quis augue tristique cursus. In suscipit id lorem pharetra euismod. Vivamus rutrum est eget dui facilisis rhoncus. Suspendisse blandit ac dui et elementum. Phasellus suscipit turpis nisl, nec condimentum ex molestie vel. Sed tincidunt ornare orci, vitae venenatis erat sodales eu. Cras aliquet fringilla ante fermentum dapibus. Vestibulum eget ullamcorper nulla. Mauris quam justo, dictum a eleifend et, molestie tempor justo. Ut in ligula vitae magna egestas interdum. Nulla quis augue ornare, tristique lacus in, tincidunt orci. Integer consequat rutrum ligula vel commodo. In sollicitudin velit eu ipsum placerat, ultricies pretium massa efficitur.</p><p>Aliquam tempor tincidunt sem, vel egestas arcu hendrerit id. Curabitur tempus nulla scelerisque sodales egestas. Fusce felis sapien, elementum sed metus lacinia, blandit mollis sem. Pellentesque vitae ligula nibh. Praesent mattis luctus mauris vel consectetur. Praesent sed urna in sapien consequat dapibus et in arcu. Nullam maximus vel arcu viverra pulvinar. Curabitur consequat ipsum in tellus scelerisque venenatis. Proin imperdiet sed mauris quis pretium. Ut ut mi rhoncus, dignissim tellus ut, varius ante. Etiam eget tortor eget quam efficitur dignissim non et velit. Ut euismod ligula at velit laoreet auctor. Aliquam nec felis id est hendrerit commodo vel sed odio. Nam non diam non magna consequat mollis. Curabitur at erat ac enim posuere lacinia mattis id elit. Donec leo lorem, lobortis a ornare non, iaculis at augue.</p><p>Phasellus viverra velit tellus, non mollis urna dignissim sit amet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam egestas eleifend commodo. Integer convallis placerat mauris, quis imperdiet arcu suscipit in. Mauris nec ante finibus, ultrices diam ullamcorper, molestie risus. Sed et interdum lectus. Etiam in malesuada eros. Nam tincidunt orci erat, ac semper mi laoreet eu. Curabitur placerat massa at ipsum egestas ornare. Praesent congue molestie fringilla. Nullam faucibus lorem libero, a placerat odio fermentum nec. Aliquam velit metus, tincidunt eget elementum eu, tincidunt sit amet tellus. Quisque luctus auctor nisi in faucibus. In hac habitasse platea dictumst.</p>", 'user');